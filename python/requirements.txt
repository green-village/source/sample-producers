avro-python3==1.8.2
certifi==2018.8.24
chardet==3.0.4
confluent-kafka==0.11.5
idna==2.7
requests==2.19.1
urllib3==1.23
