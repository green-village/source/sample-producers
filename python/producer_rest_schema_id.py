import json
import requests
import time
import base64


# Let us assume the measurement received from a device comes in a json format.
# In this example, we choose a message that represents sensor readouts from 
# a project monitoring the conditions for growing plants.
# Such message may come with the following fields:
# - timestamp
# - temperature (in degress)
# - moisture (in arbitrary units)
# - conductivity (in arbitrary units)
# - light (in arbitrary units)
measurement = {
    'timestamp': '2018-09-05 08:44:07.156370',
    'device': 'C4:7C:8D:65:BD:76',
    'temperature': 22.8,
    'moisture': 20,
    'conductivity': 356,
    'light': 48
}

# The received message needs to be converted into the generic schema.
value = {
    "metadata": {
        # Name of the project (or a unique project ID).
        # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
        "project_id": "plant_monitor",
        # Name of the device (unique within the project).
        # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
        "device_id": "test",
        # Human-readable description of the device.
        "description": "plant monitor at home",
        # Optional field with the device manufacturer (string or null).
        # For the optional fields, it is necessary to provide the value (None in this case)
        # together with the type (null in this case).
        "manufacturer": {"null": None},
        # Optional field with the device serial number (string or null).
        "serial": {"null": None},
        # Optional field with the time when the device was installed (long or null).
        "placement_timestamp": {"null": None},
        # Optional field describing the location where the device is placed (string or null).
        "location": {"null": None},
        # Optional field with the latitude in decimal degrees of the device location (float or null).
        "latitude": {"null": None},
        # Optional field with the longitude in decimal degrees of the device location (float or null).
        "longitude": {"null": None},
        # Optional field with the altitude in meters above the mean sea level.
        "altitude": {"null": None},
        # Manual topic partition assignment (int or null).
        # You can decide to send the message to a specific topic partition.
        # The partition will be assigned automatically when null is provided.
        "topic_partition": {"null": None}
    },
    "data": {
        # Time of the measurement (in milliseconds since the epoch).
        # In this example, we choose to overwrite the time from the initial json with the current time,
        # as we assume that the message arrived immediately after the measurement was read.
        # Converting the received time to milliseconds since the epoch is also a valid approach.
        "timestamp": int(time.time() * 1e3),  # time in ms
        # record_values is an array of all sensor measurements
        "values": [
            {
                # Name of the measurement.
                # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
                "name": "temperature",
                # Human-readable description of the measurement
                "description": "temperature",
                # Unit
                "unit": "ºC",
                # Type of the measured value. The options are: NULL, BOOLEAN, INT, LONG, FLOAT, DOUBLE, STRING.
                "type": "FLOAT",
                # The actual measured value.
                # It is necessary to provide the value together with the type (null, boolean, int, long, float, double, string).
                "value": {"float": measurement["temperature"]} 
            },
            {
                "name": "moisture",
                "description": "moisture",
                "unit": "",
                "type": "INT",
                "value": {"int": measurement["moisture"]}
            },
            {
                "name": "light",
                "description": "light",
                "unit": "",
                "type": "INT",
                "value": {"int": measurement["light"]}
            },
            {
                "name": "conductivity",
                "description": "conductivity",
                "unit": "",
                "type": "INT",
                "value": {"int": measurement["conductivity"]}
            },
        ]
    }
}

# The actual data that will be sent to Kafka REST Proxy consists of the value schema and measurement(s).
data = {
    # Reference ID of the generic schema stored in the Schema Registry.
    "value_schema_id": 1,
    # Array of measurements. We are providing only one measurement here.
    "records": [
        {
            "value": value
        }
    ]
}

print(json.dumps(data, indent=4, sort_keys=True))


# The Kafka REST Proxy credentials can be passed in the header.
# The format is a base64-encoded username:password.
auth_plain = "demo1-kafka-rest-user:demo1-kafka-rest-password"
auth_base64 = base64.b64encode(auth_plain.encode()).decode("utf-8")
headers = {
    "Content-Type": "application/vnd.kafka.avro.v2+json",
    "Accept": "application/vnd.kafka.v2+json",
    "Authorization": "Basic {}".format(auth_base64)
}

url = "http://gv-demo1-rest.projects.sda.surfsara.nl/topics/demo1_schema"

# Send the message with a POST request.
r = requests.post(url, json=data, headers=headers)

print(r.text)
print(r.status_code)
