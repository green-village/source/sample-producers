from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
import json
import pathlib
import avro.schema
import time


# Location of the avro schema files.
parent_dir = pathlib.Path(__file__).parent.resolve()
base_schema_filename = str(parent_dir / "schema" / "GreenVillageRecord.avsc") 
data_schema_filename = str(parent_dir / "schema" / "GreenVillageRecordData.avsc") 
meta_schema_filename = str(parent_dir / "schema" / "GreenVillageRecordMetadata.avsc") 

# Load json from the avro schema files.
data_json = json.loads(open(data_schema_filename, "r").read())
meta_json = json.loads(open(meta_schema_filename, "r").read())
base_json = json.loads(open(base_schema_filename, "r").read())

# Load nested schemas into a RecordSchema object.
names = avro.schema.Names()

data_schema = avro.schema.SchemaFromJSONData(data_json, names)
meta_schema = avro.schema.SchemaFromJSONData(meta_json, names)
base_schema = avro.schema.SchemaFromJSONData(base_json, names)

value_schema = json.dumps(base_schema.to_json(), indent=4, sort_keys=True)
print(value_schema)


# Let us assume the measurement received from a device comes in a json format.
# In this example, we choose a message that represents sensor readouts from 
# a project monitoring the conditions for growing plants.
# Such message may come with the following fields:
# - timestamp
# - temperature (in degress)
# - moisture (in arbitrary units)
# - conductivity (in arbitrary units)
# - light (in arbitrary units)
measurement = {
    'timestamp': '2018-09-05 08:44:07.156370',
    'device': 'C4:7C:8D:65:BD:76',
    'temperature': 22.8,
    'moisture': 20,
    'conductivity': 356,
    'light': 48
}

value = {
    "metadata": {
        # Name of the project (or a unique project ID).
        # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
        "project_id": "plant_monitor",
        # Name of the device (unique within the project).
        # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
        "device_id": "test",
        # Human-readable description of the device.
        "description": "plant monitor at home",
        # Optional field with the device manufacturer (string or null).
        # Optional fields can be left out.
        #"manufacturer": None,
        # Optional field with the device serial number (string or null).
        "serial": None,
        # Optional field with the time when the device was installed (long or null).
        "placement_timestamp": None,
        # Optional field describing the location where the device is placed (string or null).
        "location": None,
        # Optional field with the latitude in decimal degrees of the device location (float or null).
        "latitude": None,
        # Optional field with the longitude in decimal degrees of the device location (float or null).
        "longitude": None,
        # Optional field with the altitude in meters above the mean sea level.
        #"altitude": None,
        # Manual topic partition assignment (int or null).
        # You can decide to send the message to a specific topic partition.
        # The partition will be assigned automatically when null is provided.
        "topic_partition": None
    },
    "data": {
        # Time of the measurement (in milliseconds since the epoch).
        # In this example, we choose to overwrite the time from the initial json with the current time,
        # as we assume that the message arrived immediately after the measurement was read.
        # Converting the received time to milliseconds since the epoch is also a valid approach.
        "timestamp": int(time.time() * 1e3),  # time in ms
        # record_values is an array of all sensor measurements
        "values": [
            {
                # Name of the measurement.
                # This should be a compact name as it will be used as a part of the time series name in InfluxDB.
                "name": "temperature",
                # Human-readable description of the measurement
                "description": "temperature",
                # Unit
                "unit": "ºC",
                # Type of the measured value. The options are: NULL, BOOLEAN, INT, LONG, FLOAT, DOUBLE, STRING.
                "type": "FLOAT",
                # The actual measured value.
                "value": measurement["temperature"]
            },
            {
                "name": "moisture",
                "description": "moisture",
                "unit": "",
                "type": "INT",
                "value": measurement["moisture"]
            },
            {
                "name": "light",
                "description": "light",
                "unit": "",
                "type": "INT",
                "value": measurement["light"]
            },
            {
                "name": "conductivity",
                "description": "conductivity",
                "unit": "",
                "type": "INT",
                "value": measurement["conductivity"]
            },
        ]
    }
}

print(json.dumps(value, indent=4, sort_keys=True))


avroProducer = AvroProducer({
        'bootstrap.servers': ', '.join(['gv-kafka-kafka-{}.gv-kafka-broker.ns-green-village.svc.cluster.local:9092'.format(i) for i in range(7)]),
        'schema.registry.url': 'http://gv-schemaregistry-schema-registry.ns-green-village.svc.cluster.local:8081',
        #"debug": "security",
        "security.protocol": "SASL_PLAINTEXT",
        'sasl.mechanisms': 'GSSAPI',
        "sasl.kerberos.service.name": "kafka",
        "sasl.kerberos.principal": "base@THEGREENVILLAGE.ORG",
        "sasl.kerberos.keytab": "base.keytab"
    }, default_value_schema=base_schema)


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


# Trigger any available delivery report callbacks from previous produce() call
avroProducer.poll(0)

# Asynchronously produce a message, the delivery report callback
# will be triggered from poll() above, or flush() below, when the message has
# been successfully delivered or failed permanently.
avroProducer.produce(topic='base_schema', value=value, callback=delivery_report)

# Wait for any outstanding messages to be delivered and delivery report
# callbacks to be triggered.
avroProducer.flush()
