from confluent_kafka import Producer
import json


# Let us assume the measurement received from a device comes in a json format.
# In this example, we choose a message that represents sensor readouts from 
# a project monitoring the conditions for growing plants.
# Such message may come with the following fields:
# - timestamp
# - temperature (in degress)
# - moisture (in arbitrary units)
# - conductivity (in arbitrary units)
# - light (in arbitrary units)
measurement = {
    'timestamp': '2018-09-05 08:44:07.156370',
    'device': 'C4:7C:8D:65:BD:76',
    'temperature': 22.8,
    'moisture': 20,
    'conductivity': 356,
    'light': 48
}

value = json.dumps(measurement)


producer = Producer({
    'bootstrap.servers': ', '.join(['gv-kafka-kafka-{}.gv-kafka-broker.ns-green-village.svc.cluster.local:9092'.format(i) for i in range(7)]),
    #"debug": "security",
    "security.protocol": "SASL_PLAINTEXT",
    'sasl.mechanisms': 'GSSAPI',
    "sasl.kerberos.service.name": "kafka",
    "sasl.kerberos.principal": "base@THEGREENVILLAGE.ORG",
    "sasl.kerberos.keytab": "base.keytab"
    })


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


# Trigger any available delivery report callbacks from previous produce() call
producer.poll(0)

# Asynchronously produce a message, the delivery report callback
# will be triggered from poll() above, or flush() below, when the message has
# been successfully delivered or failed permanently.
producer.produce('base_raw', value=value, callback=delivery_report)

# Wait for any outstanding messages to be delivered and delivery report
# callbacks to be triggered.
producer.flush()
