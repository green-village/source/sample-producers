import json
import requests
import base64


# Let us assume the measurement received from a device comes in a json format.
# In this example, we choose a message that represents sensor readouts from 
# a project monitoring the conditions for growing plants.
# Such message may come with the following fields:
# - timestamp
# - temperature (in degress)
# - moisture (in arbitrary units)
# - conductivity (in arbitrary units)
# - light (in arbitrary units)
measurement = {
    'timestamp': '2018-09-05 08:44:07.156370',
    'device': 'C4:7C:8D:65:BD:76',
    'temperature': 22.8,
    'moisture': 20,
    'conductivity': 356,
    'light': 48
}

data = {"records": [{"value": measurement}]}


# The Kafka REST Proxy credentials can be passed in the header.
# The format is a base64-encoded username:password.
auth_plain = "demo1-kafka-rest-user:demo1-kafka-rest-password"
auth_base64 = base64.b64encode(auth_plain.encode()).decode("utf-8")
headers = {
    "Content-Type": "application/vnd.kafka.json.v2+json",
    "Accept": "application/vnd.kafka.v2+json",
    "Authorization": "Basic {}".format(auth_base64)
}

url = "http://gv-demo1-rest.projects.sda.surfsara.nl/topics/demo1_raw"

# Send the message with a POST request.
r = requests.post(url, json=data, headers=headers)

print(r.text)
print(r.status_code)
