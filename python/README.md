# Example Python Producer

This section shows how to produce messages from Python.
Examples are given for:
- producing raw messages using `Producer` from `confluent_kafka` 
- producing raw messages using Kafka REST Proxy via Python `requests`
- producing messages in the avro generic schema using `AvroProducer` from `confluent_kafka.avro` 
- producing messages in the avro generic schema using Kafka REST Proxy via Python `requests`

```
pip install -r requirements.txt
```

## Confluent-kafka

The examples utilizing the confluent-kafka package use Kerberos authentication to connect to Kafka.
The pre-built Linux wheels do not contain SASL Kerberos/GSSAPI support. If you need SASL Kerberos/GSSAPI support you must install librdkafka and its dependencies and then build confluent-kafka.
For this, librdkafka needs to be compiled with libsasl2 or openssl support, the cyrus-sasl-dev and openssl-dev packages are needed for this.
In order to facilitate using these examples right away, [Dockerfile](Dockerfile) with the full installation process is supplied.

The following files need to be supplied to make this example work for your project:
- `krb5.conf`
- Kerberos keytab (`base.keytab` in this example)

The following settings need to be checked in the python files:
- Kafka bootstrap servers and the schema registry url
- Kerberos principal and keytab
- Kafka topic  

## REST Proxy

The Kafka REST Proxy is deployed for each project and uses the Kerberos authentication with the corresponding project principal and keytab.
This is already specified when the REST Proxy is deployed and the user does not have to take care of this.
For users, the REST Proxy is secured using basic authentication.

The following settings need to be checked in the python files:
- Kafka REST Proxy url including the Kafka topic
- Username and password for the basic authentication
