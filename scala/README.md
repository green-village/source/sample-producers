# Example Scala Producer

Simple producer written in Scala. This producer will, if configured correctly, produce a record with three values: the
Sine, Cosine and Tangent of the timestamp the record is created. 

## Build

Install the following packages when running on Ubuntu.

```
apt-get install openjdk-8-jdk krb5-user
```

Gradle is used for building and packaging.

```
./gradlew installDist
```

## Run

Change the settings in `sample.conf`, `jaas.conf` and `krb5.conf` as required.

Set the `SCALA_PRODUCER_OPTS` environment variable.

```
export SCALA_PRODUCER_OPTS="-Djava.security.krb5.conf=krb5.conf -Djava.security.auth.login.config=jaas.conf"
```

Run the application.

```
build/install/scala-producer/bin/scala-producer sample.conf
```
