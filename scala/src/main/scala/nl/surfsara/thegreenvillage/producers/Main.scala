package nl.surfsara.thegreenvillage.producers

import java.io.File
import java.util.concurrent.Executors
import java.util.{Properties, Timer}

import com.typesafe.config.{Config, ConfigFactory}
import grizzled.slf4j.Logging
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.thegreenvillage.record.GreenVillageRecord

/**
  * Main entrypoint
  *
  * @author Mathijs Kattenberg - mathijs.kattenberg@surfsara.nl
  */
object Main extends Logging {
  // Threadpool for running producer loop
  val executor = Executors.newCachedThreadPool()

  // Singleton inner object for config
  object ConfigContext {
    var config = ConfigFactory.load()
  }

  /**
    * Main entry point. Starts the producer
    *
    * @param args Arguments to the program. One arg is optional: a config file to use (see resources/reference.conf)
    */
  def main(args: Array[String]): Unit = {
    // Load and init config
    loadConfig(args)
    val config = ConfigContext.config
    val sampleConfig = config.getConfig("sampleproducer")
    val kafkaConfig = config.getConfig("sampleproducer.kafkasettings")

    val producer = new KafkaProducer[String, GreenVillageRecord](initKafkaProducerProperties(kafkaConfig))

    val pollTaskTimer = new Timer("producer timer")
    val topic = kafkaConfig.getString("topic")
    val projectId = sampleConfig.getString("project_id")
    val sensorId = sampleConfig.getString("record_source_id")
    val pollTask = new PollTask(projectId, sensorId, producer, topic)
    val period = sampleConfig.getLong("interval") * 1000
    pollTaskTimer.schedule(pollTask, 0L, period)
  }

  /**
    * Initialize a Properties object from a config file for constructing a producer
    *
    * @param kafkaConfig The Config to use
    * @return A Properties object with Kafka producer properties from the config file
    */
  def initKafkaProducerProperties(kafkaConfig: Config): Properties = {
    val props = new Properties
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getStringList("bootstrap_servers"))
    props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaConfig.getString("client_id"))
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.KafkaAvroSerializer")
    props.put("security.protocol", kafkaConfig.getString("security_protocol"))
    props.put("sasl.kerberos.service.name", "kafka")
    props.put("schema.registry.url", kafkaConfig.getString("schema_registry"))
    props
  }

  /**
    * Load the config, are use reference config (resources/reference.conf)
    *
    * @param args The arguments to the program. The config file to use can be specified as first argument
    */
  def loadConfig(args: Array[String]): Unit = {
    var configFileArg = ""
    if (args.length >= 1) {
      configFileArg = args(0)
    }
    val configFile = new File(configFileArg)
    val configFromFile = ConfigFactory.parseFile(configFile)
    ConfigContext.config = ConfigFactory.load(configFromFile)
    ConfigContext.config.checkValid(ConfigFactory.defaultReference(), "sampleproducer")
  }

}
