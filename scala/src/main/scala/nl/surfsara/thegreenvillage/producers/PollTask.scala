package nl.surfsara.thegreenvillage.producers

import java.time.Instant
import java.util.TimerTask

import grizzled.slf4j.Logging
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.thegreenvillage.record.{GreenVillageRecord, GreenVillageRecordMetadata, GreenVillageRecordData, value, `type`}

import scala.collection.JavaConverters._

class PollTask(projectId: String, sensorId: String, producer: KafkaProducer[String, GreenVillageRecord], topic: String) extends TimerTask with Logging {

  override def run(): Unit = {
    val timeStamp = Instant.now()
    val schemaData = buildValues(projectId, sensorId)
    producer.send(new ProducerRecord(topic, sensorId, schemaData))
  }

  def buildValues(projectId: String, sensorId: String): GreenVillageRecord = {
    val metaData = GreenVillageRecordMetadata.newBuilder()
      .setProjectId(projectId)
      .setDeviceId(sensorId)
      .setDescription("Sample record")
      .setManufacturer("Sample")
      .setSerial(null)
      .setPlacementTimestamp(1483225200000L)
      .setLocation("Somewhere")
      .setLatitude(52.498006F)
      .setLongitude(4.768434F)
      .setAltitude(null)
      .setTopicPartition(null)
      .build()

    val time = Instant.now().toEpochMilli
    var values = Vector.empty[value]

    values = values :+ value.newBuilder()
      .setName("sin(t)")
      .setDescription("sin(record timestamp)")
      .setUnit("sin(t)")
      .setType(`type`.DOUBLE)
      .setValue(scala.math.sin(time))
      .build()

    values = values :+ value.newBuilder()
      .setName("cos(t)")
      .setDescription("cos(record timestamp)")
      .setUnit("cos(t)")
      .setType(`type`.DOUBLE)
      .setValue(scala.math.cos(time))
      .build()

    values = values :+ value.newBuilder()
      .setName("tan(t)")
      .setDescription("tan(record timestamp)")
      .setUnit("tan(t)")
      .setType(`type`.DOUBLE)
      .setValue(scala.math.tan(time))
      .build()

    val data = GreenVillageRecordData.newBuilder()
      .setTimestamp(Instant.now().toEpochMilli)
      .setValues(values.asJava)
      .build()

    val gvrecord = GreenVillageRecord
      .newBuilder()
      .setMetadata(metaData)
      .setData(data)
      .build()
    gvrecord
  }
}
