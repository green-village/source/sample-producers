# Producing Messages with Kafka REST Proxy

This section shows how to produce messages using Kafka REST Proxy.

https://docs.confluent.io/current/kafka-rest/docs/intro.html

For each project, Kafka REST Proxy is configured with the Kerberos keytab belonging to the project. In this way, only the topics from the project can be accessed.
The examples are given using `curl` from the command line. The [python](../python) directory gives examples on how to produce messages with Kafka REST Proxy from Python scripts.

With the command below, you will be able to see topics from all projects. However, you can only publish/subscribe to the topics for which you have the access rights.

```
curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics \
    -u base-kafka-rest-user:1234 | jq .
```

Show particular topics.

```
curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics/base_raw \
    -u base-kafka-rest-user:1234 | jq .

curl -X GET https://gv-base-rest.projects.sda.surfsara.nl/topics/base_schema \
    -u base-kafka-rest-user:1234 | jq .
```



## Raw messages

Produce a message to the `base_raw` topic.

```
curl -X POST -H "Content-Type: application/vnd.kafka.json.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    --data '{"records":[{"value":{"temperature": 42}}]}' "https://gv-base-rest.projects.sda.surfsara.nl/topics/base_raw" \
    -u base-kafka-rest-user:1234 | jq .
```

Alternatively, the credentials can be passed in the authorization header in a base64-encoded format consisting of username:password.

```
printf '%s' "base-kafka-rest-user:1234" | base64
```

```
curl -X POST -H "Content-Type: application/vnd.kafka.json.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    --data '{"records":[{"value":{"temperature": 42}}]}' "https://gv-base-rest.projects.sda.surfsara.nl/topics/base_raw" \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .
```



## Messages in the Avro schema

Produce a message to the `base_schema` topic.
The [data.json](data.json) file shows an example where the generic schema is sent along with the measurement.

```
curl -X POST -H "Content-Type: application/vnd.kafka.avro.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    --data @data.json "https://gv-base-rest.projects.sda.surfsara.nl/topics/base_schema" \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .
```

The [data2.json](data2.json) file shows an example where the generic schema is referenced using its ID from the Schema Registry.

```
curl -X POST -H "Content-Type: application/vnd.kafka.avro.v2+json" \
    -H "Accept: application/vnd.kafka.v2+json" \
    --data @data2.json "https://gv-base-rest.projects.sda.surfsara.nl/topics/base_schema" \
    -H "Authorization: Basic YmFzZS1rYWZrYS1yZXN0LXVzZXI6MTIzNA==" | jq .
```
